import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeadComponent } from './head/head.component';
import { UnitListComponent } from './unit-list/unit-list.component';
import { ListComponent } from './list/list.component';
import { CustomTimePipe } from './pipes/custom-time.pipe';

@NgModule({
  declarations: [
    HeadComponent,
    UnitListComponent,
    ListComponent,
    CustomTimePipe,
  ],
  imports: [CommonModule],
})
export class FeedsModule {}
