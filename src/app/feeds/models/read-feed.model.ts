export interface ReadFeadModel {
  _id: number;
  story_title: string;
  title: string;
  story_url: string;
  url: string;
  author: string;
  created_at: string;
}
