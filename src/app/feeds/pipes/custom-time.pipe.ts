import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'customTime',
})
export class CustomTimePipe implements PipeTransform {
  constructor(private datePipe: DatePipe) {}
  transform(value: string): string {
    const date = new Date(value);
    const now = new Date();
    const nowFullDay = now.getDay();
    const dateFullDay = date.getDay();
    if (nowFullDay === dateFullDay) {
      return this.datePipe.transform(date, 'hh:mm aaa');
    } else {
      if (
        Math.abs(nowFullDay - dateFullDay) === 1 ||
        Math.abs(nowFullDay - dateFullDay) === 6
      ) {
        return 'Yesterday';
      } else {
        return this.datePipe.transform(date, 'LLL dd');
      }
    }
  }
}
