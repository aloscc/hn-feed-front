import { Component, OnInit, Input } from '@angular/core';
import { ReadFeadModel } from '../models/read-feed.model';
import { FeedsService } from '../feeds.service';

@Component({
  selector: 'app-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.css'],
})
export class UnitListComponent implements OnInit {
  @Input() feed: ReadFeadModel;
  showFeed = true;

  constructor(private feedsService: FeedsService) {}

  ngOnInit(): void {
    if (this.feed.title === null && this.feed.story_title === null) {
      this.showFeed = false;
    }
  }
  goToLink(): void {
    const url = this.feed.url || this.feed.story_url;
    window.open(url, '_blank');
  }
  removeFeed(): void {
    this.feedsService.deleteFeed(this.feed._id).subscribe(() => {
      this.showFeed = false;
    });
  }
}
