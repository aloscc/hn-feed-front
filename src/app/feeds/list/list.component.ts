import { Component, OnInit } from '@angular/core';
import { FeedsService } from '../feeds.service';
import { ReadFeadModel } from '../models/read-feed.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  feeds: ReadFeadModel[];
  constructor(private feedsService: FeedsService) {}

  ngOnInit(): void {
    this.feedsService.getFeeds().subscribe(({ data }) => {
      this.feeds = data;
    });
  }
}
