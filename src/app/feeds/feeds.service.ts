import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FeedsService {
  readonly baseUrl = `${environment.baseUrl}/feeds`;
  constructor(private http: HttpClient) {}

  getFeeds(): Observable<any> {
    return this.http.get<any>(this.baseUrl);
  }

  deleteFeed(feedId: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${feedId}`);
  }
}
