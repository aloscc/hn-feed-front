import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './feeds/list/list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'feeds',
    pathMatch: 'full',
  },
  {
    path: 'feeds',
    component: ListComponent,
    children: [
      {
        path: 'feeds',
        loadChildren: () =>
          import('./feeds/feeds.module').then((m) => m.FeedsModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
