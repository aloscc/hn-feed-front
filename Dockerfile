FROM node:lts-alpine
WORKDIR /usr/src/app_front

COPY . .
RUN npm install

EXPOSE 4200 

## Launch the wait tool and then your application
CMD npm run start:prod
